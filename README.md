1. Build rootfs:

`./build`

2. Start container:

`./run`

3. Attach natively ...

`./attach`

... or using Web browser (Chrome will work, Firefox might not work)

`./proxy`

(Change size of window as a proof that the container was really snapshotted.)

4. Freeze/snapshot container (will not work if connection from Xpra client/Web browser is still active):

`./freeze`

5. Restore container:

`./restore`

6. Access again:

`./proxy`
